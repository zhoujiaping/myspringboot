# MySpringBoot

学习springboot

启动 
* $ mvn spring-boot:run
* $ java -jar target/myapplication-0.0.1-SNAPSHOT.jar --my.name=zjp
* $ java -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=8000,suspend=n -jar target/myapplication-0.0.1-SNAPSHOT.jar

1. webflux
2. 统一异常处理
3. jdbctemplate
4. 连接池
5. 优雅停机
6. cache
7. 多环境配置
    开发环境直接启动即可，配置了 spring.profiles.active=dev
    测试环境 java -jar myapplication-0.0.1-SNAPSHOT.jar --spring.profiles.active=test
    生产环境 java -jar myapplication-0.0.1-SNAPSHOT.jar --spring.profiles.active=prod
    生产环境，配置外置
8. log
9. session


