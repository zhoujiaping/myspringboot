drop table if exists t_tiny_url;
drop table if exists t_tiny_url_occupy;
create table if not exists t_tiny_url(
    tiny_url_id bigint primary key auto_increment comment '主键',
    tiny_url varchar(32) unique comment '短网址',
    long_url varchar(767) unique comment '长网址',
    version int(11) default 0,
    key t_tiny_url_idx_tiny_url(tiny_url),
    key t_tiny_url_idx_long_url(long_url)
) comment '短网址';
create table if not exists t_tiny_url_occupy(
    tiny_url_occupy_id bigint primary key auto_increment comment '主键',
    tiny_url varchar(32) unique comment '短网址',
    occupy_id bigint unique comment '占用该短网址的id',
    version int(11) default 0,
    key t_tiny_url_occupy_idx_tiny_url(tiny_url),
    key t_tiny_url_occupy_idx_occupy_id(occupy_id)
) comment '短网址被id的占用关系';