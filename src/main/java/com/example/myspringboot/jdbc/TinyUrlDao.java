package com.example.myspringboot.jdbc;

import com.example.myspringboot.pojo.TinyUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Repository
public class TinyUrlDao extends AbstractJdbcDao<Long, TinyUrl>{
    @Autowired
    DataSource dataSource;
    @PostConstruct
    public void initDs(){
        setDataSource(dataSource);
    }

}
