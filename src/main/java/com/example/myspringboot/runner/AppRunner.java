package com.example.myspringboot.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

/**应用启动后执行*/
@Component
@Slf4j
public class AppRunner implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) {
        try{
            run0(args);
        }catch (Exception e){
            log.error("AppRunner#run error",e);
        }
    }
    private void run0(ApplicationArguments args) throws Exception {
        String arguments = args.getOptionNames().stream().map(name ->
                name + "=" + args.getOptionValues(name).stream()
                        .collect(Collectors.joining(","))
        ).collect(Collectors.joining(" "));
        log.info("args:{}", arguments);

        //if(1==1)return;
        var latch = new CountDownLatch(1);

        WebClient.builder().baseUrl("http://localhost:8080/").build()
                .get().retrieve().bodyToMono(String.class).subscribe(body -> {
            log.info("index content:{}", body);
            latch.countDown();
        });
        latch.await();
    }
}
