package com.example.myspringboot.pojo;

import lombok.*;

@Data
@EqualsAndHashCode
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TinyUrlOccupy {
    private Long tinyUrlOccupyId;
    private String tinyUrl;
    private Long occupyId;
}
