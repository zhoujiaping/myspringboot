package com.example.myspringboot.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 访问应用参数
 * */
@Component
@Slf4j
public class AppArgAccessDemo {
    @Autowired
    public AppArgAccessDemo(ApplicationArguments args) {
        boolean debug = args.containsOption("debug");
        List<String> nonOptionArgs = args.getNonOptionArgs();
        log.info("args:{}",String.join(" ",nonOptionArgs));
        // if run with "--debug logfile.txt" debug=true, files=["logfile.txt"]
    }
}
