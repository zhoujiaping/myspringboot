package com.example.myspringboot.util;

public abstract class SqlUtils {
    public static String concat(String... parts){
        if(parts==null){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for(String part : parts){
            sb.append(part).append(" ");
        }
        return sb.toString();
    }
}
