package com.example.myspringboot.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

/**应用事件监听器*/
public class MyListener<E extends ApplicationEvent> implements ApplicationListener {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        logger.info("app event:{}",applicationEvent);
    }
}
